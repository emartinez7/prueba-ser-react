import './App.css';
import React, {useEffect} from 'react';
import { Route, Redirect, Switch } from "react-router-dom";
import NotFound from "./components/NotFound";
import ListaEmpleados from "./components/ListaEmpleados";
import Formulario from "./components/Formulario";
import ConnectApi from './components/ConnectApi';

function App() {

  useEffect(()=>{
    console.log("INIT APP.JS");
  }, []);

      

  return (
    <main className="App">
      <Switch>
        <Route path="/" exact={true} render = {props => (
          <ListaEmpleados {...props} />
        )}/>
        <Route path="/nuevo" render = {props => (
          <Formulario {...props} />
        )}/>
        <Route path="/:id/editar" render = {props => (
          <Formulario {...props} />
        )}/>
        <Route path="/connect" render = {props => (
          <ConnectApi {...props} />
        )}/>
        <Route path="/not-found" component={NotFound} />
        <Redirect to="/not-found" />

      </Switch>
    </main>
  );
}

export default App;
