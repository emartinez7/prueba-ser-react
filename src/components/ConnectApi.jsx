import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import Notiflix from "notiflix-react";

const ConnectApi = () => {
  const [dato, setDato] = useState({});

  useEffect( ()=>{
    console.log("Rendered");
    Notiflix.Loading.Pulse('Conectando...');
    Notiflix.Loading.Remove(3000);
    callData();
  }, []);

  function callData(){
    let url = "https://datos.gob.es/apidata/nti/territory/Province?_sort=label&_pageSize=5&_page=0";
    fetch(url)
    .then(res => res.json())
    .catch( err => {
      console.log("error api ",err);
    }).then(response => {
      console.log("Response........ ",response);
      setDato(response.result.items[1]);
      let aux = response.result.items[1];
      for(let k in aux){
        console.log(" K " + k + " | valor " + aux[k]);
      }
    });
  }

  return (
    <React.Fragment>
      <h1>Conectando API</h1>
      <div>
        <Link to="/" className="btn btn-link">Inicio</Link>
      </div>
      <div>
        <pre>
          {
            Object.keys(dato).map( k=>
              ( 
                  '"'+ k + '" : "' + dato[k] + '"' + "\n"
            
            ) )
          }
        </pre>
        <br/>
        <pre>
          Hola, probando
          no formateado?
          Extraño
        </pre>
      </div>
    </React.Fragment>
  );
  
};

export default ConnectApi;