import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import {
  addEmployee, editEmployee
} from "../utils/fakeService";
import _cons from "../utils/constantes";
import Notiflix from "notiflix-react";

const Formulario = (props) => {
  const [employee, setEmployee] = useState(clear());
  const [cedula, setCedula] = useState("");
  var mode = props.location.mode;

  useEffect(()=>{
    clear();
    if(props.location.mode === undefined){
      props.history.push("/");
    }
    console.log("empleado ", props.location.emp);
    if(props.location.mode === "edit"){
      
      setEmployee(props.location.emp);
    }
  }, []);

  function clear(){
    let obj = {
      id:0,firstName:"", lastName:"", email:"", cedula:"", inss:"", birthday:""
    }
    return obj;
  }

  function add(){
    let response = addEmployee(employee);
    if(response.code === _cons.ok_employee[0]){
      props.history.push("/");
    }
    else{
      //show notiflix respone.message
      Notiflix.Report.Failure( 'Agregar Empleado',
        response.message, 'Cerrar' ); 
      console.log("message" , response.message);
    }
  }

  function edit(){
    let response = editEmployee(employee);
    if(response.code === _cons.ok_employee[1]){
      props.history.push("/");
    }
    else{
      //show notiflix respone.message
      Notiflix.Report.Failure( 'Editar Empleado',
        response.message, 'Cerrar' ); 
    }

  }

  function processCedula(valor, keyCode) {
    if(valor.length <= 15 ){
      // solo numeros
      if ((keyCode >= 96 && keyCode <= 105) || (keyCode >= 48 && keyCode <= 57)){
        if(valor.length === 4){
          let f = valor.substring(3);
          let s = valor.substring(0,3);
          valor = s + "-" + f;
        }
        if(valor.length === 10){
          let f = valor.substring(10);
          let s = valor.substring(0,10);
          valor = s + "-" + f;
        }

      }
      else if(keyCode !== 8 && keyCode !== 46){
        valor = valor.substring(0,valor.length-1);
      }

    }
    else if(valor.length === 16){
      // Solo letras
      if(keyCode < 65 || keyCode > 90){
        valor = valor.substring(0,15);
      }
      else{
        let f = valor.substring(15).toUpperCase();
        valor = valor.substring(0,15) + f;
      }
    }
    else{
     valor = valor.substring(0,15);
    }
    return valor;
  }

  function processDate(valor){
    let aux = valor.substr(4,6);
    let anio = extractYear(aux);
    let y = anio.toString();
    let m = aux.substr(2,2);
    let d = aux.substr(0,2);
    let f = y + "-" + m + "-" + d;
    let fecha = new Date(f);
    if( isNaN(fecha.getTime()) ){
      // No es fecha. Show notificacion
      f = "";
    }
    console.log("Fecha... ",f);
    return f;
  }

  function extractYear(valor){
    let year = valor.substr(4,2);
    let anio = 2000;
    if(parseInt(year) <= 99 && parseInt(year) >= 10){
      anio = parseInt("19" + year);
    }
    else{
      anio += parseInt(year);
    }
    return anio;
  }

  function validate(){
    let valid = true;
    let notiflixBody = '<ul class="noti-warning">'
    if(employee.firstName.length === 0){
      valid = false;
      notiflixBody += '<li> Escribir el nombre </li>';
      //show notiflix
    }
    if(employee.lastName.length === 0){
      valid = false;
      notiflixBody += '<li> Escribir los apellidos </li>';
    }
    if(employee.email.length === 0){
      valid = false;
      notiflixBody += '<li> Ingresar un email válido </li>';
    }
    if(employee.cedula.length < 16){
      valid = false;
      notiflixBody += '<li> Escriba la cédula </li>';
    }
    if(employee.inss.length === 0){
      valid = false;
      notiflixBody += '<li> Ingresar el número del INSS </li>';
    }
    if(!valid){
      notiflixBody += '</ul>';
      console.log("body ", notiflixBody);
      Notiflix.Report.Warning( 'Revise los datos', notiflixBody, 'Cerrar' ); 
    }
    return valid;
  }

  const hndChangeText = ({ currentTarget : input }) =>{
    let emp = {...employee};
    emp[input.name] = input.value;
    setEmployee(emp);
  }

  const hndKeyCedula = ({currentTarget: input, keyCode: keyNum, target: t}) => {
    let valor = cedula;
    let emp = {...employee};
    let newValue = processCedula(valor, keyNum);
    emp.cedula = newValue;
    if(valor.length === 16){
      let f = processDate(valor);
      //setTimeout(function(){
        console.log("fecha cumple ", f);
        
        if(f.length > 0){
          emp.birthday = f;
        }
        setEmployee(emp);
      //},1500);
    }
    else{
      setEmployee(emp);
    }
    
  }

  const hndChangedCedula = ({currentTarget: input})=>{
    setCedula(input.value);
  }

  const cancel = () =>{
    if(mode === "add"){
      setEmployee(clear);
    }
    else{
      setEmployee(props.location.emp);
    }
    
  }

  const saveEmployee = () =>{
    let t = validate();
    if(t){
      if(mode === "add"){
        add();
      }
      else{
        edit();
      }
      
    }
  }

  return ( 
    <React.Fragment>
      {props.location.mode === "add" ? <h1>Agregar Empleado</h1> : <h1>Editar Empleado</h1>} 
      <main>
        <div>
          <Link to="/" className="btn btn-link">Regresar</Link>
        </div>
        <div className="input-group">
          <input type="text" className="texto" name="firstName" placeholder="Nombres" maxLength="50" value={employee.firstName} onChange={hndChangeText}/>
        </div>
        <div className="input-group">
          <input type="text" className="texto" name="lastName" placeholder="Apellidos" maxLength="50" value={employee.lastName} onChange={hndChangeText}/>
        </div>
        <div className="input-group">
          <input type="email" className="texto" readOnly={props.location.mode === "edit"} name="email" placeholder="example@mail.com" maxLength="40" value={employee.email} onChange={hndChangeText}/>
        </div>
        <div className="input-group">
          <input type="text" className="texto" name="inss" placeholder="Seguro Social" maxLength="40" value={employee.inss} onChange={hndChangeText}/>
        </div>
        <div className="input-group">
          <input type="text" onChange ={hndChangedCedula} className="texto" placeholder="cédula" maxLength="20" value={employee.cedula} onKeyUp={hndKeyCedula}/>
        </div>
        <div className="input-group">
          <input type="date" className="texto" placeholder="example@mail.com"  value={employee.birthday} readOnly={true}/>
        </div>
        <div className="buttons">
          <button className="btn btn-accept" type="button" onClick={saveEmployee}>Guardar</button>
          <button className="btn btn-cancel" type="button" onClick={cancel}>Cancelar</button>
        </div>
      </main>
    </React.Fragment>
  );
};

export default Formulario;