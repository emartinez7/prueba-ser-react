import React, {useState, useEffect} from "react";
//import _cons from "../utils/constantes";
import { Link } from "react-router-dom";
import {
  getEmployees, deleteEmployee
} from "../utils/fakeService";
import Notiflix from "notiflix-react";

const ListaEmpleados = () => {
   const [mounted, setMounted] = useState(false);
   const [lista, setLista] = useState(getEmployees());
   const [aux, setAux] = useState(999);

   useEffect(()=>{
    Notiflix.Report.Init({plainText:false});
    Notiflix.Confirm.Init({plainText:false});
     listEmployees();
   }, [aux]);

   function listEmployees(){
     let arr = getEmployees();
     setLista(arr);
     setMounted(true);
   }

   const preDelete = (employee) => {
    let nombre = employee.firstName + " " + employee.lastName;
    Notiflix.Confirm.Show( 'Borrar Empleado', '¿Desea borrar al empleado <h2> <strong>' + nombre + ' </strong> </h2> ?', 'Sí, borrar', 'Cancelar'
      ,function() { // Yes callback
          //borrar
          console.log(employee);
          deleteEmployee(employee.id);
          setAux(aux - 1);
        }
        ,function(){ // No callback
          //Cerrar normal
        } 
    ); 
   }
   
  return mounted && (
    <React.Fragment>
      <h1>Empleados</h1>
      <main>
        <Link to={ {pathname:"/nuevo", mode:"add"} } className="btn btn-link" >Nuevo</Link>
        <table className="table">
          <thead>
            <tr>
            <th>Nombres</th>
            <th>Apelldos</th>
            <th>Email</th>
            <th>INSS</th>
            <th>Cédula</th>
            <th>Fecha Nac.</th>
            <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            {
              lista.map(x => 
                 (<tr key={x.cedula}>
                  <td> {x.firstName} </td>
                  <td> {x.lastName} </td>
                  <td> {x.email} </td>
                  <td> {x.inss} </td>
                  <td> {x.cedula} </td>
                  <td> {x.birthday} </td>
                  <td> <Link to={{ pathname:"/" + x.id + "/editar", mode:"edit", emp:x} } className="btn-s btn-accept">Editar</Link> 
                    <button className="btn-s btn-cancel" type="button" onClick={() =>{preDelete(x)} }>Borrar</button>
                  </td>
                </tr>)
               )
            }
          </tbody>
        </table>
        <Link to="/connect" className="btn btn-link" >Conectar API prueba</Link>
      </main>
    </React.Fragment>
  );

};

export default ListaEmpleados;