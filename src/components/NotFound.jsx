import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <React.Fragment>
      <h1>Página no encontrada...</h1>;
      <Link to="/" className="btn btn-link">Regresar</Link>
    </React.Fragment>
  );
   
};

export default NotFound;