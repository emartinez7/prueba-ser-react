import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import { BrowserRouter } from "react-router-dom";

console.log("Index JS");

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  // <React.StrictMode>
    
  // </React.StrictMode>,
  document.getElementById('root')
);
