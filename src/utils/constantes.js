const _cons = {
  lista: "employees",
  error_employee:[91,92,93,94],
  message_error:["Ya existe un empleado con este correo", "Ya existe un empleado con este número de INSS",
    "Ya existe un empleado con esta cédula", "No existe el empleado"],
  ok_employee:[11, 12],
  message_ok: ["Empleado guardado exitosamente","Empleado editado exitosamente"]


};


export default _cons;