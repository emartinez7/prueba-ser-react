import _cons from "../utils/constantes";

export function getEmployees (){
  let dato = localStorage.getItem(_cons.lista);
  if(dato === null){
    dato = [];
    localStorage.setItem(_cons.lista, JSON.stringify(dato));
  }
  return JSON.parse(dato);
}

export function editEmployee(obj) {
  let validation = validateEmployee(obj, true);
  if(validation.code !== 0){
    return validation;
  }
  let lista = getEmployees();
  let e = lista.find(x => x.id === obj.id);
  if(e !== undefined){
    e.firstName = obj.firstName;
    e.lastName = obj.lastName;
    e.inss = obj.inss;
    e.cedula = obj.cedula;
    e.birthday = obj.birthday;
    localStorage.setItem(_cons.lista, JSON.stringify(lista));
    return {code:_cons.ok_employee[1], message:_cons.message_ok[1]};
  }
  return {code: _cons.error_employee[3], message:_cons.message_error[3]};
}

export function addEmployee(obj) {
  let validation = validateEmployee(obj);
  if(validation.code !== 0){
    return validation;
  }
  let lista = getEmployees();
  let id = lista.length + 1;
  obj.id = id;
  lista.push(obj);
  localStorage.setItem(_cons.lista, JSON.stringify(lista));
  return {code:_cons.ok_employee[0], message:_cons.message_ok[0]};
}

function validateEmployee(obj, editing){
  let lista = getEmployees();
  if(editing === true){
    lista= lista.filter(x => x.id !== obj.id);
  }
  let e = lista.find(x => x.email === obj.email);
  if(e !== undefined){
    return {code:_cons.error_employee[0], message:_cons.message_error[0] + ": <strong>" + obj.email + "</strong>"};
  }
  e = lista.find(x => x.inss.toString() === obj.inss.toString());
  if(e !== undefined){
    return {code:_cons.error_employee[1], message:_cons.message_error[1] + ": <strong>" + obj.inss + "</strong>"};
  }
  e = lista.find(x => x.cedula.toLowerCase() === obj.cedula.toLowerCase());
  if(e !== undefined){
    return {code:_cons.error_employee[2], message:_cons.message_error[2] + ": <strong>" + obj.cedula + "</strong>"};
  }
  return {code:0, message:"ok"};
}

export function deleteEmployee(id){
  let lista = getEmployees();
  let aux = lista.filter(x => x.id !== id);
  lista[0].firstName= "Samanta";
  localStorage.setItem(_cons.lista, JSON.stringify(aux));
}
